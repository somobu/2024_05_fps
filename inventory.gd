class_name Inventory
extends Node

class Item:
	var id: String
	var count: int
	var data: LootEntry
	
	func save() -> Dictionary:
		return {
			"id": id,
			"count": count,
		}
	
	func load(tree: Node, d: Dictionary):
		id = d.id
		count = d.count
		data = tree.get_node("/root/Root/LootTable/" + id)


var items:Array[Item] = []


func save() -> Array:
	var d = []
	
	for item in items:
		d.push_back(item.save())
	
	return d


func load(d: Array):
	items.clear()
	for item in d:
		var i = Item.new()
		i.load(self, item)
		items.push_back(i)


func add_item(id: String, count: int):
	for item in items:
		if item.id == id:
			item.count += count
			return
	
	var item = Item.new()
	item.id = id
	item.data = get_node("/root/Root/LootTable/" + id)
	item.count = count
	items.push_back(item)


func take_item(id: String, count: int):
	for item in items:
		if item.id == id:
			item.count -= count
			
			if item.count <= 0:
				items.erase(item)
			
			return


func add_to_min(id: String, count: int):
	for item in items:
		if item.id == id:
			item.count = max(item.count, count)
			return
	
	add_item(id, count)


func has_items_in_category(cat: LootEntry.Type) -> bool:
	for item in items:
		if item.data.entry_type == cat:
			return true
	return false


func get_quantity(id: String) -> int:
	for item in items:
		if item.id == id:
			return item.count
	
	return 0


func has_some(id: String) -> bool:
	return get_quantity(id) > 0
