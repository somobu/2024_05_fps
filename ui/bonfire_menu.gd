extends Control

@onready var root: Root = get_node("/root/Root")


func _unhandled_input(event: InputEvent) -> void:
	if not visible: return
	
	if event.is_action_released("cancel"):
		root.hide_menus()
	
	if event.is_action_pressed("move_bwd"):
		$ItemList.select(min($ItemList.get_selected_items()[0] + 1, $ItemList.item_count - 1))
	
	if event.is_action_pressed("move_fwd"):
		$ItemList.select(max(0, $ItemList.get_selected_items()[0] - 1))



func reload(bonfire_name: String):
	$Title.text = "Currently at: " + str(bonfire_name)
	
	$ItemList.clear()
	
	for bonfire in root.known_bonfires:
		$ItemList.add_item(bonfire.human_name)
	
	$ItemList.select(0)
	$ItemList.grab_focus()


func _on_item_list_item_activated(index: int) -> void:
	self.visible = false
	root.warp_to_bonfire_short.bind(root.known_bonfires[index]).call_deferred()
