extends Control


@onready var root: Root = get_node("/root/Root")


func show_menu(entry_name: String) -> void:
	var entry: LootEntry = get_node("/root/Root/LootTable/" + entry_name)
	$Panel/Label.text = entry.human_name
	$Panel/TextureRect.texture = entry.icon
	self.visible = true
	
	$AudioStreamPlayer.play()


func _unhandled_input(event: InputEvent) -> void:
	if not visible: return
	
	if event.is_action_released("jump"):
		root.hide_menus()
