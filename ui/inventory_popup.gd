extends Control

@onready var root: Root = get_node("/root/Root")
@onready var player: Player = get_node("/root/Root/Player")
@onready var inv: Inventory = get_node("/root/Root/Player/Inventory")

var current_item_list_items = []

var inv_pick_mode = false
var inv_pick_filter = null
var inv_pick_args = [null, null]

func _ready() -> void:
	bind_equip_signals()


func show_menu():
	recalc_tabs()
	
	self.visible = true
	$Equipment.visible = true
	$Panel.visible = false
	$PickModeOverlay.visible = false
	$TabBar.current_tab = 1
	_on_tab_bar_tab_changed(1)


func bind_equip_signals():
	for mode in ["Quick", "Armor", "Melee", "Ranged", "Explosive", "Tool"]:
		for i in range(1, 5):
			var btn = get_node_or_null("Equipment/%s%d" % [mode, i])
			if btn == null:
				break
			
			btn.connect("focus_entered", _equip_focus.bind(mode, i))
			btn.connect("pressed", _equip_press.bind(mode, i))


func _unhandled_input(event: InputEvent) -> void:
	if not visible: return
	
	if event.is_action_pressed("inventory") or event.is_action_pressed("cancel"):
		if inv_pick_mode:
			reset_pick_mode()
		else:
			root.hide_menus()
		
		return
	
	if event.is_action_pressed("ui_accept"):
		if inv_pick_mode and $Panel/ItemList.get_selected_items().size() > 0:
			inv_pick_success(current_item_list_items[$Panel/ItemList.get_selected_items()[0]])
	
	if not inv_pick_mode:
		# Move tab selection to the left, skipping disabled tabs
		if event.is_action_pressed("inv_prev"):
			for i in range($TabBar.current_tab - 1, 0, -1):
				if not $TabBar.is_tab_disabled(i):
					$TabBar.current_tab = i
					break
		
		# Move tab selection to the right, skipping disabled tabs
		if event.is_action_pressed("inv_next"):
			for i in range($TabBar.current_tab + 1, $TabBar.tab_count - 1, +1):
				if not $TabBar.is_tab_disabled(i):
					$TabBar.current_tab = i
					break
	
	var list: ItemList = $Panel/ItemList
	if $Panel.visible and list.item_count > 0:
		if event.is_action_pressed("ui_down"):
			var idx = min(list.get_selected_items()[0] + 1, list.item_count - 1)
			list.select(idx)
			_on_item_list_item_selected(idx)
		
		if event.is_action_pressed("ui_up"):
			var idx = max(0, list.get_selected_items()[0] - 1)
			list.select(idx)
			_on_item_list_item_selected(idx)


func _on_item_list_item_selected(index: int) -> void:
	var item = current_item_list_items[index]
	$ItemDescr/Title.text = item.data.human_name
	$ItemDescr/Descr.text = item.data.description



func _on_tab_bar_tab_changed(tab: int) -> void:
	match tab:
		1:
			$Equipment.visible = true
			$Panel.visible = false
			recalc_equip()
		2:
			$Equipment.visible = false
			$Panel.visible = true
			recalc_item_list([LootEntry.Type.Consume])
		3:
			$Equipment.visible = false
			$Panel.visible = true
			recalc_item_list([LootEntry.Type.Key])
		4:
			$Equipment.visible = false
			$Panel.visible = true
			recalc_item_list([LootEntry.Type.Weapon])
		5:
			$Equipment.visible = false
			$Panel.visible = true
			recalc_item_list([]) # TODO: armor
		_:
			$Equipment.visible = false
			$Panel.visible = true
			recalc_item_list([])


func recalc_tabs():
	$TabBar.set_tab_disabled(2, not inv.has_items_in_category(LootEntry.Type.Consume))
	$TabBar.set_tab_disabled(3, not inv.has_items_in_category(LootEntry.Type.Key))
	$TabBar.set_tab_disabled(4, not inv.has_items_in_category(LootEntry.Type.Weapon))
	$TabBar.set_tab_disabled(5, not inv.has_items_in_category(LootEntry.Type.Armor))


func recalc_equip():
	$Equipment/Quick1.grab_focus()
	$ItemDescr/No.visible = false
	
	for mode in ["Quick", "Armor", "Melee", "Ranged", "Explosive", "Tool"]:
		for i in range(1, 5):
			var btn = get_node_or_null("Equipment/%s%d" % [mode, i])
			if btn == null:
				break
			
			var icon: TextureRect = get_node("Equipment/%s%d/TextureRect" % [mode, i])
			var item = item_by_mode_index(mode, i)
			
			if item != null and inv.has_some(item):
				icon.texture = root.get_loot(item).icon
			else:
				icon.texture = null


func items_list_by_mode(mode: String):
	match mode:
		"Quick":
			return player.quick_use_items
		"Armor":
			return player.equipped_armor
		"Melee":
			return player.chosen_melee
		"Ranged":
			return player.chosen_ranged
		"Explosive":
			return player.chosen_explosives
		"Tool":
			return player.chosen_tools
		_:
			assert(false, "Wrong mode")
			return []


func item_by_mode_index(mode: String, i: int):
	return items_list_by_mode(mode)[i - 1]


func recalc_item_list(filter):
	var filter_func = func (item): 
		return item.data.entry_type in filter
	
	recalc_item_list_callable(filter_func)


func recalc_item_list_callable(filter: Callable):
	var list: ItemList = $Panel/ItemList
	list.clear()
	current_item_list_items.clear()
	
	for item in inv.items:
		if not filter.call(item):
			continue
		
		assert(item.count > 0)
		
		var count_suffix = "" if item.count == 1 else " x" + str(item.count)
		list.add_item(item.data.human_name + count_suffix, item.data.icon)
		current_item_list_items.push_back(item)
	
	$Panel/ItemList/No.visible = list.item_count == 0
	$ItemDescr/No.visible = list.item_count == 0
	$ItemDescr/Title.text = ""
	$ItemDescr/Descr.text = ""
	
	if list.item_count > 0:
		list.select(0)
		_on_item_list_item_selected(0)


func _equip_focus(mode: String, index: int) -> void:
	var item_id = item_by_mode_index(mode, index)
	
	if item_id != null and inv.has_some(item_id):
		var item = root.get_loot(item_id)
		$ItemDescr/Title.text = item.human_name
		$ItemDescr/Descr.text = item.description
	else:
		$ItemDescr/Title.text = "No item assigned"
		$ItemDescr/Descr.text = ""
	
	$ItemDescr/No.visible = false


func _equip_press(mode: String, index: int) -> void:
	inv_pick_args[0] = mode
	inv_pick_args[1] = index
	inv_pick_mode = true
	
	var filter
	var category
	
	match mode:
		"Quick":
			filter = func(item: Inventory.Item):
				return item.data.entry_type == LootEntry.Type.Consume
		"Armor":
			if index == 1:
				filter = func(item: Inventory.Item):
					return item.data.entry_subtype == LootEntry.SubType.Armor_Helmet
			else:
				filter = func(item: Inventory.Item):
					return item.data.entry_subtype == LootEntry.SubType.Armor_Vest
		"Melee":
			filter = func(item: Inventory.Item):
				return item.data.entry_subtype == LootEntry.SubType.Weapon_Melee
		"Ranged":
			filter = func(item: Inventory.Item):
				return item.data.entry_subtype == LootEntry.SubType.Weapon_Ranged
		"Explosive":
			filter = func(item: Inventory.Item):
				return item.data.entry_subtype == LootEntry.SubType.Weapon_Explosive
		"Tool":
			filter = func(item: Inventory.Item):
				return item.data.entry_subtype == LootEntry.SubType.Weapon_Tool
		_:
			assert(false)
	
	recalc_item_list_callable(filter)
	
	if current_item_list_items.size() <= 0:
		$SoundWrongDisabled.play()
		reset_pick_mode()
		return
	
	$Equipment.visible = false
	$Panel.visible = true
	$PickModeOverlay.visible = true


func reset_pick_mode():
	inv_pick_mode = false
	$PickModeOverlay.visible = false
	$Panel.visible = false
	$Equipment.visible = true
	$TabBar.current_tab = 1
	_on_tab_bar_tab_changed(1)
	get_node("Equipment/%s%d" % inv_pick_args).grab_focus()
	


func inv_pick_success(item: Inventory.Item):
	var target_idx = inv_pick_args[1] - 1
	
	var list = items_list_by_mode(inv_pick_args[0])
	
	if item.id in list:
		var prev_idx = list.find(item.id)
		list[prev_idx] = null
	
	list[target_idx] = item.id
	
	match inv_pick_args[0]:
		"Quick":
			player.recalc_quick_items()
	
	reset_pick_mode()
