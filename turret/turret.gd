extends RigidBody3D

signal waked_up

## Head rotation speed, rad/s
const HEAD_SPEED = 1.5

## Arm rotation speed, rad/s
const ARM_SPEED = 2.8

@onready var root: Root = get_node("/root/Root")
@onready var player: Player = get_node("/root/Root/Player")

var mat_inactive = preload("res://turret/turret_inactive.tres")
var mat_warning = preload("res://turret/turret_warning.tres")
var mat_alarm = preload("res://turret/turret_alarm.tres")
var mat_ok = preload("res://turret/turret_ok.tres")
var mat_dead = preload("res://turret/turret_dead.tres")

@export var hp = 3.0
@export var souls: int = 10

## How far turret wakes up
@export var sense_radius = 4.8

@export var follow_distance = 5.0
@export var fire_distance = 3.0

@export var is_sleeping = true

## How long turret be idle before go to sleep
@export var _sleep_timeout = 45.0

@export var alarm_colldown = 10.0

## Shots per second
@export var fire_rate = 5.0
@export var bullet_damage = 1.0
@export var dispersion = 5.0

var last_known_player_point = Vector3.ZERO

var initial_fire_cooldown = 0

var sleep_timeout = 0
var in_aggro = false

@export var weapon_left = "cannon"
@export var weapon_right = "cannon2"


func _ready() -> void:
	$Head/Indicator.material = mat_inactive
	$Sense/CollisionShape3D.shape = $Sense/CollisionShape3D.shape.duplicate(true)
	$Sense/CollisionShape3D.shape.radius = sense_radius
	
	$Head/ArmLeft/Cannon.fire_rate = fire_rate
	$Head/ArmLeft/Cannon.bullet_damage = bullet_damage
	$Head/ArmLeft/Cannon.dispersion_scale = dispersion
	$Head/ArmLeft/Cannon.set_visual(root.get_loot(weapon_left).weapon_visual)
		
	$Head/ArmRight/Cannon.fire_rate = fire_rate
	$Head/ArmRight/Cannon.bullet_damage = bullet_damage
	$Head/ArmRight/Cannon.dispersion_scale = dispersion
	$Head/ArmRight/Cannon.set_visual(root.get_loot(weapon_right).weapon_visual)


func _physics_process(delta: float) -> void:
	if is_sleeping:
		return
	
	if hp > 0:
		actions(delta)


func actions(delta: float):
	var pl_pos_loc = to_local(player.global_position + Vector3(0, 0.7, 0))
	var in_fire_dist = false
	
	var rc: RayCast3D = $PlayerFollow
	rc.target_position = pl_pos_loc
	
	if rc.get_collider() is Player:
		initial_fire_cooldown += delta
		
		var dist = (global_position - rc.get_collision_point()).length()
		
		if dist > follow_distance:
			$Head/Indicator.material = mat_ok
			sleep_timeout += delta
		elif dist > fire_distance and not in_aggro:
			$Head/Indicator.material = mat_warning
			last_known_player_point = pl_pos_loc
			sleep_timeout = 0
		else:
			$Head/Indicator.material = mat_alarm
			last_known_player_point = pl_pos_loc
			in_fire_dist = true
			sleep_timeout = 0
		
	else:
		$Head/Indicator.material = mat_warning
		initial_fire_cooldown = 0
		sleep_timeout += delta
	
	var is_rotating = false
	var head_angle_to_player = -Vector2.RIGHT.angle_to(Vector2(last_known_player_point.x, last_known_player_point.z))
	is_rotating = abs(angle_difference(head_angle_to_player, $Head.rotation.y)) > 0.001
	$Head.rotation.y = rotate_toward($Head.rotation.y, head_angle_to_player, HEAD_SPEED * delta)
	
	var arm_angle = PI/2 - Vector3.UP.angle_to(last_known_player_point)
	is_rotating = is_rotating or abs(angle_difference(arm_angle, $Head/ArmLeft.rotation.x)) > 0.001
	arm_angle = rotate_toward($Head/ArmLeft.rotation.x, arm_angle, ARM_SPEED * delta)
	$Head/ArmLeft.rotation.x = arm_angle
	$Head/ArmRight.rotation.x = arm_angle
	
	if is_rotating:
		if not $ServoSound.playing:
			$ServoSound.play()
	else:
		$ServoSound.playing = false
	
	var passed_init_cd = initial_fire_cooldown > 0.5
	var targeted = angle_difference($Head.rotation.y, head_angle_to_player) < 0.01
	
	if in_fire_dist and targeted and passed_init_cd:
		if not $Head/ArmLeft/Cannon.firing:
			$Head/ArmLeft/Cannon.fire_cooldown = 0.15
		
		$Head/ArmLeft/Cannon.firing = true
		$Head/ArmRight/Cannon.firing = true
	else:
		$Head/ArmLeft/Cannon.firing = false
		$Head/ArmRight/Cannon.firing = false
	
	if sleep_timeout > _sleep_timeout:
		go_to_sleep()



func wake_up():
	if is_sleeping:
		is_sleeping = false
		$PlayerFollow.enabled = true
		emit_signal("waked_up")


func go_to_sleep():
	is_sleeping = true
	in_aggro = false
	$PlayerFollow.enabled = false
	$Head/Indicator.material = mat_inactive


func _on_sense_body_entered(body: Node3D) -> void:
	if body is Player:
		wake_up()


func projectile_damage(value: float):
	if hp > 0:
		hp -= value
		in_aggro = true
		
		if hp > 0 and is_sleeping:
			wake_up()
		
		if hp <= 0:
			$Head/ArmLeft/Cannon.firing = false
			$Head/ArmRight/Cannon.firing = false
			$ServoSound.playing = false
			$Head/Indicator.material = mat_dead
			
			$ShortCircuitSound.play()
			$ShortCircuitParticles.visible = true
			$ShortCircuitParticles.emitting = true
			
			var tween = create_tween()
			tween.tween_callback(player.give_souls.bind(souls)).set_delay(0.6)
