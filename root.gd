class_name Root
extends Node3D

@export var load_savefile = true

@export_category("Bonfire data")
@export var default_bonfire_level = "canals"
@export var default_bonfire_id = "canal_access"

var death_sphere = preload("res://elements/death_sphere.tscn")
@onready var land_particles = preload("res://elements/land_particles.tscn")

## Fully blocking (blacked-out) menu is shown
## Restrict all player actions
var is_blocking_menu = false

## Partially blocking menu (popup) is shown
## Do not allow player to jump, run and fire
var is_semi_blocking = false


var known_bonfires = []
var last_bonfire: Bonfire.Data = null


var persist_data = {}


func _ready() -> void:
	if load_savefile:
		load_save_data()
	
	$Blackout.modulate = Color.WHITE
	$BonfireMenu.visible = false
	
	if last_bonfire == null:
		warp_to_bonfire_short(get_default_bonfire())
	else:
		warp_to_bonfire_short(last_bonfire)


func load_save_data():
	var file = FileAccess.open("user://savedata.json", FileAccess.READ)
	var data = JSON.parse_string(file.get_as_text())
	
	if data != null:
		known_bonfires.clear()
		for bf in data["known_bonfires"]:
			var bfd = Bonfire.Data.new()
			bfd.load(bf)
			known_bonfires.push_back(bfd)
		
		persist_data = data["persist"]
		
		$Player.load(data["player"])
		
		last_bonfire = Bonfire.Data.new()
		last_bonfire.load(data["last_bonfire"])
		
		if data["death_sphere"]["souls"] > 0:
			place_death_sphere(
				data["death_sphere"]["souls"], 
				Vector3(data["death_sphere"]["x"], data["death_sphere"]["y"], data["death_sphere"]["z"])
			)


func save_data():
	var data = {
		"known_bonfires": [],
		"persist": persist_data,
		"player": $Player.save(),
		"last_bonfire": null,
		"death_sphere": null,
	}
	
	for bf in known_bonfires:
		data.known_bonfires.push_back(bf.save())
	
	if last_bonfire != null:
		data.last_bonfire = last_bonfire.save()
	else:
		data.last_bonfire = get_default_bonfire().save()
	
	var old_inst = get_node_or_null("DeathShpere")
	if old_inst != null:
		data.death_sphere = {
			"souls": old_inst.souls,
			"x": old_inst.global_position.x,
			"y": old_inst.global_position.y,
			"z": old_inst.global_position.z,
		}
	else:
		data.death_sphere = {
			"souls": 0,
			"x": 0,
			"y": -10000,
			"z": 0,
		}
	
	var file = FileAccess.open("user://savedata.json", FileAccess.WRITE)
	file.store_string(JSON.stringify(data, "  "))


func get_loot(name: String) -> LootEntry:
	return get_node("LootTable/%s" % name)


func get_default_bonfire() -> Bonfire.Data:
	var bd = Bonfire.Data.new()
	bd.level = default_bonfire_level
	bd.id = default_bonfire_id
	bd.human_name = "Default bonfire"
	return bd



func add_spawn_point_to_known(data: Bonfire.Data):
	if is_bonfire_activated(data):
		return
	
	known_bonfires.push_back(data)
	
	var c_sort = func(a,b):
		return a.human_name < b.human_name
	known_bonfires.sort_custom(c_sort)


func is_bonfire_activated(data: Bonfire.Data):
	for bf in known_bonfires:
		var m0 = data.level == bf.level
		var m1 = data.id == bf.id
		if m0 and m1:
			return true
	
	return false


func show_bonfire_menu(bonfire: Bonfire.Data):
	is_blocking_menu = true
	last_bonfire = bonfire
	
	var tween = get_tree().create_tween()
	if $Blackout.modulate != Color.WHITE:
		tween.tween_property($Blackout, "modulate", Color.WHITE, 0.4)
	
	var actually_show = func():
		$BonfireMenu.visible = true
		$BonfireMenu.reload(bonfire.human_name)
		
	tween.tween_callback(actually_show)


func on_item_pick_up(entry_name: String):
	is_semi_blocking = true
	$Player.release_aim()
	$PickedUpPopup.show_menu(entry_name)
	
	var player_inv: Inventory = $Player/Inventory
	player_inv.add_item(entry_name, 1)
	
	$Player.recalc_quick_items()


func show_player_inventory():
	is_blocking_menu = true
	$InventoryPopup.show_menu()


func refill_estus_flasks():
	var player_inv: Inventory = $Player/Inventory
	player_inv.add_to_min("estus", 3)
	$Player.recalc_quick_items()


func hide_menus():
	if is_blocking_menu:
		var tween = get_tree().create_tween()
		tween.tween_property($Blackout, "modulate", Color.TRANSPARENT, 0.2)
	
	$BonfireMenu.visible = false
	$PickedUpPopup.visible = false
	$InventoryPopup.visible = false
	
	is_blocking_menu = false
	is_semi_blocking = false


func warp_to_bonfire(bonfire: Bonfire.Data):
	warp_sequence(bonfire, exit_blackout)
	hide_menus()


func warp_to_bonfire_short(bonfire: Bonfire.Data):
	warp_sq_unload(bonfire, exit_blackout)
	hide_menus()


func death_sequence():
	var gpos = $Player.last_safe_pos
	var souls = $Player/PlayerStats.souls
	
	$Player.reset_death_vars()
	
	var tween = create_tween()
	tween.tween_callback(refill_estus_flasks).set_delay(0.6)
	
	# Death orb have to be placed before warp
	# Or else it wont be saved correctly during warp
	place_death_sphere(souls, gpos)
	
	warp_sequence(last_bonfire, exit_blackout)
	hide_menus()


func place_death_sphere(souls: int, gpos: Vector3):
	var old_inst = get_node_or_null("DeathShpere")
	if old_inst != null:
		remove_child(old_inst)
		old_inst.queue_free()
	
	var inst: DeathSphere = death_sphere.instantiate()
	inst.name = "DeathShpere"
	add_child(inst)
	
	inst.souls = souls
	inst.global_position = gpos


func warp_sequence(bonfire: Bonfire.Data, on_warp_end: Callable):
	is_blocking_menu = true
	
	var tween = get_tree().create_tween()
	tween.tween_property($Blackout, "modulate", Color.WHITE, 0.4)
	tween.tween_callback(warp_sq_unload.bind(bonfire, on_warp_end)).set_delay(0.5)


func warp_sq_unload(bonfire: Bonfire.Data, on_warp_end: Callable):
	
	# Rm all projectiles and that kind of stuff
	get_tree().call_group("lvl_tmp", "queue_free")
	
	# Move player somewhere far away to avoid spawn aggro
	$Player.global_position = Vector3(0, 2500, 0)
	
	$LevelLoader.unload_all()
	
	var tween = get_tree().create_tween()
	tween.tween_callback(warp_sq_reset_player.bind(bonfire, on_warp_end)).set_delay(0.01)


func warp_sq_reset_player(bonfire: Bonfire.Data, on_warp_end: Callable):
	is_blocking_menu = true
	last_bonfire = bonfire
	
	$LevelLoader.load_level(bonfire.level)
	
	print("Warp ends at ", bonfire.level, " ", bonfire.id)
	
	var player: Player = $Player
	player.global_transform = get_node(bonfire.level).get_node(bonfire.id).get_spawn_transform()
	player.reset_vars() # Reset vars AFTER glob pos is set!
	
	is_blocking_menu = false
	
	save_data()
	
	on_warp_end.call_deferred()


func exit_blackout():
	var tween = get_tree().create_tween()
	tween.tween_callback(func(): pass).set_delay(0.2)
	tween.tween_property($Blackout, "modulate", Color.TRANSPARENT, 0.4)
