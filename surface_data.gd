class_name SurfaceData
extends Node

@export var steps_sound: AudioStream
@export var landing_effect: Resource

@export var bullet_sound: AudioStream
