extends Area3D

@onready var root: Root = get_node("/root/Root")

func action_get_name() -> String:
	return "Warp to hub"


func action_perform():
	var bd = Bonfire.Data.new()
	bd.level = "canals"
	bd.id = "canal_access"
	root.warp_to_bonfire(bd)
