extends Area3D

var door_open = false

func action_get_name() -> String:
	if door_open:
		return ""
	else:
		return "Open the door"


func action_perform():
	if door_open:
		pass
	else:
		# TODO: show 'Locked by key' popup
		pass
