extends Area3D

signal wake_up


func _on_body_entered(body: Node3D) -> void:
	if body is Player:
		emit_signal("wake_up")
