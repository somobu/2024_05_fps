class_name Player
extends CharacterBody3D

@export var undead = false

const MOUSE_SENS = Vector2(0.002, 0.002) # Vector2(0.005, 0.005)
const GAMEPAD_SENS = Vector2(0.05, 0.05)

var max_cam_height = 1.5
var max_collider_height = 1.8

## ctrl
var crouch_height = 0.9

## ctr + shift. Cannot set lower that 0.7 due to 
var low_crouch_height = 0.7

var target_height = max_collider_height

var own_is_on_floor = false
var was_on_floor = true
@onready var prev_pos = self.global_position
var jump_pressed = false


const LANDING_EFFECT_DELAY = 0.3
var landing_effect_delay = 0

const FOOTSTEP_DELAY = 0.6
var footstep_delay = 0

var sprint_toggled = false


@onready var root: Root = get_node("../")
@onready var stats: PlayerStats = $PlayerStats


## Data of geomentry player currently stands on
var footsteps_data: SurfaceData = null


## Last position on solid ground -- here we'll spawn orb on death
var last_safe_pos: Vector3 = Vector3.ZERO


var quick_use_items = [ "estus", "first_aid", null, null ]

var chosen_melee = [ null, null ]
var chosen_ranged = [ "cannon", "cannon2", null, null ]
var chosen_explosives = [ null, null ]
var chosen_tools = [ null, null ]


## ID of item equipped to right hand
var hand_right = null

## ID of item equipped to left
var hand_left = null
var equipped_armor = [ null, null ]

var initial_fov = 0
var initial_hand_right_pos = Vector3.ZERO
var initial_hand_left_pos = Vector3.ZERO

var is_in_aim = false
var is_right_hand_aim = true


func save() -> Dictionary:
	var d = {
		"quick_use_items": quick_use_items,
		"chosen_ranged": chosen_ranged,
		"hand_right": hand_right,
		"hand_left": hand_left,
		"inv": $Inventory.save(),
		"souls": $PlayerStats.souls,
	}
	
	return d


func load(d: Dictionary):
	quick_use_items = d.quick_use_items
	chosen_ranged = d.chosen_ranged
	hand_right = d.hand_right
	hand_left = d.hand_left
	
	$Inventory.load(d.inv)
	$PlayerStats.souls = d.souls
	$UI/Souls/Label.text = str($PlayerStats.souls)
	
	recalc_quick_items()
	recalc_weapons()


func _ready() -> void:
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	max_cam_height = $CamV.position.y
	max_collider_height = $CollisionShape3D.shape.height
	($CollisionShape3D/ShapeCast3D as ShapeCast3D).add_exception(self)
	($CollisionShape3D/ShapeCastDown as ShapeCast3D).add_exception(self)
	
	$UI/Crosshair.position = $UI.size / 2
	$UI/Souls/Label.text = str($PlayerStats.souls)
	
	initial_fov = $CamV/Camera3D.fov
	initial_hand_left_pos = $CamV/Camera3D/ArmLeft.position
	initial_hand_right_pos = $CamV/Camera3D/ArmRight.position
	
	recalc_quick_items()


func _input(event: InputEvent) -> void:
	if not stats.is_dead() and not root.is_blocking_menu:
		if event is InputEventMouseMotion:
			self.rotation.y -= event.relative.x * MOUSE_SENS.x
			$CamV.rotation.x -= event.relative.y * MOUSE_SENS.y
			$CamV.rotation.x = clampf($CamV.rotation.x, -0.5*PI, 0.5*PI)
		
		if not root.is_semi_blocking:
			if event.is_action_pressed("jump"):
				jump_pressed = true
			
			if event.is_action_released("jump"):
				jump_pressed = false
			
			if event.is_action_pressed("inventory"):
				get_viewport().set_input_as_handled()
				root.show_player_inventory()
			
			if event.is_action_pressed("quick_use_1"):
				consume_quick_item(0)
			
			if event.is_action_pressed("quick_use_2"):
				consume_quick_item(1)
			
			if event.is_action_pressed("quick_use_3"):
				consume_quick_item(2)
			
			if event.is_action_pressed("quick_use_4"):
				consume_quick_item(3)
			
			if event.is_action_pressed("weapon_melee"):
				pass
			
			if event.is_action_pressed("weapon_ranged"):
				choose_weapon(chosen_ranged, Input.is_action_pressed("weapon_mod_alt"))
			
			if event.is_action_pressed("aim"):
				start_aim()
			
			if event.is_action_pressed("fire") and not has_weapon_right():
				$SoundWrongDisabled.play()
		
		if event.is_action_released("toggle_sprint"):
			sprint_toggled = not sprint_toggled
		
		if event.is_action_released("toggle_flashlight"):
			$CamV/Camera3D/Flashlight.visible = not $CamV/Camera3D/Flashlight.visible
	
	if event.is_action_released("aim"):
		release_aim()


var prev_vel_hist = [
	Vector3.ZERO, Vector3.ZERO, Vector3.ZERO, Vector3.ZERO, 
	Vector3.ZERO, Vector3.ZERO, Vector3.ZERO, Vector3.ZERO, 
]


func _physics_process(delta: float) -> void:
	
	# Tricky fuckery: is_on_floor implementation is crappy:
	# in fall damage condition velocity is not maximal
	# so we keep history of last few frames to detect maximal velocity
	prev_vel_hist.remove_at(0)
	prev_vel_hist.push_back((global_position - prev_pos) / delta)
	
	var prev_velocity: Vector3 = prev_vel_hist[0]
	for v in prev_vel_hist:
		if v.length() > prev_velocity.length():
			prev_velocity = v
		
	prev_pos = global_position


	# Gravity
	if not is_on_floor():
		velocity.y -= 9.8 * delta
	
	
	# Own floor detection
	var down_shape = $CollisionShape3D/ShapeCastDown as ShapeCast3D
	if down_shape.get_collision_count() > 0:
		var dist = (down_shape.global_position - down_shape.get_collision_point(0)).y
		own_is_on_floor = (dist <= target_height * 0.51)
	else:
		own_is_on_floor = false
	
	
	# Query surface data
	footsteps_data = null
	
	var down_collider = $CollisionShape3D/RayCastDown.get_collider()
	if down_collider != null and down_collider is Node3D:
		footsteps_data = down_collider.get_node_or_null("SurfaceData")
	
	if footsteps_data == null:
		footsteps_data = $DefaultSurfaceData
	
	
	# Landing effect
	landing_effect_delay += delta
	if own_is_on_floor and not was_on_floor and prev_velocity.y < -3.0 and landing_effect_delay > LANDING_EFFECT_DELAY:
		var land_particles = footsteps_data.landing_effect
		
		if land_particles != null:
			var inst = land_particles.instantiate()
			root.add_child(inst)
			inst.global_position = global_position
			landing_effect_delay = 0
	
	
	# Fall damage
	if own_is_on_floor and not was_on_floor:
		stats.fall_damage(prev_velocity)
	
	
	# Sound when mid-air
	var should_play = velocity.length() > 6.0 # Just a bit higher than default sprint speed
	if should_play and not $WindSound.playing:
		$WindSound.play()
	
	if not should_play and $WindSound.playing:
		$WindSound.stop()

	
	if not stats.is_dead() and not root.is_blocking_menu:
		handle_movement(delta)
		
		if not root.is_semi_blocking:
			handle_use()
		
	else:
		velocity.x = lerpf(velocity.x, 0, delta * 2.5)
		velocity.z = lerpf(velocity.z, 0, delta * 2.5)
	
	was_on_floor = own_is_on_floor
	move_and_slide()
	
	# Health & Stamina bars
	var max_health_size = $UI/CharStat/HealthBg.size.x
	$UI/CharStat/HealthFg.size.x = max_health_size * stats.get_health_percent()
	
	var max_sta_size = $UI/CharStat/StaminaBg.size.x
	$UI/CharStat/StaminaFg.size.x = max_sta_size * stats.get_stamina_percent()
	$UI/LowStaminaIndicator.visible = stats.is_low_stamina()
	
	$FpsCounter/Label.text = "FPS: " + str(Engine.get_frames_per_second())
	$FpsCounter/TextureRect.visible = Engine.get_frames_per_second() < 25


func _process(delta: float) -> void:
	if not stats.is_dead() and not root.is_blocking_menu:
		
		var input_dir = Input.get_vector("look_left", "look_right", "look_up", "look_down")
		var has_input = input_dir or false
	
		self.rotation.y -= input_dir.x * GAMEPAD_SENS.x
		$CamV.rotation.x -= input_dir.y * GAMEPAD_SENS.y
		$CamV.rotation.x = clampf($CamV.rotation.x, -0.5*PI, 0.5*PI)
		
		if not root.is_semi_blocking:
			handle_weapon(delta)


func handle_movement(delta: float):
	var semi_block = root.is_semi_blocking
	var in_sprint = (Input.is_action_pressed("sprint") or sprint_toggled) and stats.get_stamina_percent() > 0.01
	
	
	# Crouch
	var target_crouch_heigt = max_collider_height
	if Input.is_action_pressed("crouch") and not semi_block:
		if Input.is_action_pressed("sprint"):
			target_crouch_heigt = low_crouch_height
		else:
			target_crouch_heigt = crouch_height
		
	
	var rc = $CollisionShape3D/ShapeCast3D as ShapeCast3D
	if rc.get_collision_count() > 0:
		var dist = (rc.get_collision_point(0) - rc.global_position).y * 2 - 0.1
		dist = floor(dist * 10) / 10.0
		target_crouch_heigt = min(target_crouch_heigt, max(low_crouch_height, dist))
	
	target_height = move_toward(target_height, target_crouch_heigt, 0.1)
	
	var crouch_percent = (target_height - low_crouch_height) / (max_collider_height - low_crouch_height)
	var is_crouch = crouch_percent < 0.7
	
	$CollisionShape3D.shape.height = target_height
	$CollisionShape3D.position.y = max_collider_height - target_height/2
	
	
	
	# Jump
	if own_is_on_floor and stats.has_stamina_for_jump() and jump_pressed and not semi_block:
		var jump_vel = stats.get_jump_velocity()
		velocity.y = jump_vel if not is_crouch else jump_vel / 3.0
		stats.consume_jump_stamina()
	
	
	
	# Horizontal movement
	var input_dir = Input.get_vector("move_left", "move_right", "move_fwd", "move_bwd").rotated(-rotation.y).normalized()
	var has_input = input_dir or false
	
	if is_on_floor():
		var target_speed = 0
		
		if has_input:
			
			if not is_crouch:
				if in_sprint and not semi_block:
					target_speed = stats.get_sprint_speed()
					stats.consume_sprint_stamina(delta, is_on_floor())
				else:
					target_speed = stats.get_move_speed()
					
			else: # We're in crouch mode
				target_speed = stats.get_crouch_speed()
		
		
		var target_velocity = input_dir * target_speed
		var change_modifier = 4.0 if has_input else 12.0
		
		velocity.x = lerpf(velocity.x, target_velocity.x, delta * change_modifier)
		velocity.z = lerpf(velocity.z, target_velocity.y, delta * change_modifier)
	else:
		var target_velocity = Vector2(velocity.x, velocity.z)
		if has_input:
			target_velocity = input_dir * (stats.get_move_speed() if not in_sprint else stats.get_sprint_speed())
		
		var change_modifier = 2.5
		velocity.x = lerpf(velocity.x, target_velocity.x, delta * change_modifier)
		velocity.z = lerpf(velocity.z, target_velocity.y, delta * change_modifier)

	# Footsteps
	footstep_delay += delta * velocity.length() * 0.33
	
	if is_on_floor() and has_input and footstep_delay > FOOTSTEP_DELAY:
		footstep_delay = 0
		$FootstepsSound.stream = footsteps_data.steps_sound
		$FootstepsSound.play()


func handle_use():
	if root.is_semi_blocking:
		$UI/ActionPerformName.visible = false
		return
	
	var rc: RayCast3D = $CamV/Camera3D/RayCastActivate
	var collider = rc.get_collider()
	
	if collider != null and collider.has_method("action_get_name"):
		$UI/ActionPerformName.visible = true
		$UI/ActionPerformName.text = collider.action_get_name() + " [F]"
		
		if Input.is_action_just_released("action_perform"):
			collider.action_perform()
			$UI/ActionPerformName.visible = false
		
	else:
		$UI/ActionPerformName.visible = false


func handle_weapon(delta: float):
	
	# Move crosshair
	var screen: Vector2 = $UI.size / 2
	var dist = -1
	if $CamV/Camera3D/RayCastAim.is_colliding():
		var global: Vector3 = $CamV/Camera3D/RayCastAim.get_collision_point()
		dist = (global - $CamV/Camera3D.global_position).length()
		
		var arm: Node3D = $CamV/Camera3D/ArmRight
		if not is_in_aim: # In aim we are not rotating weapon at all
			var local_tgt = arm.to_local(global).normalized()
			var new_tgt = lerp(Vector3.FORWARD, local_tgt, 0.2);
			
			($CamV/Camera3D/ArmRight as Node3D).look_at(arm.to_global(new_tgt))
		
		var targeted = $CamV/Camera3D/ArmRight/RayCastAim.get_collision_point()
		screen = $CamV/Camera3D.unproject_position(targeted)
	else:
		($CamV/Camera3D/ArmRight as Node3D).rotation = Vector3(0, 0, 0)
		($CamV/Camera3D/ArmLeft as Node3D).rotation = Vector3(0, 0, 0)
	
	$UI/Crosshair.position = lerp($UI/Crosshair.position, screen, 8 * delta)
	$UI/Crosshair/Distance.text = ("%0.1f" % dist) if dist > 0 else "Inf"
	
	if has_weapon_right():
		$CamV/Camera3D/ArmRight/Cannon.firing = Input.is_action_pressed("fire")


func kill() -> void:
	var tween = create_tween()
	
	var cb = func ():
		if $PlayerStats.is_dead(): return
		
		$PlayerStats.loose_health(8.0)
		kill.call_deferred()
	
	tween.tween_callback(cb)


func give_souls(value: int) -> void:
	$PlayerStats.souls += value
	$UI/Souls/Label.text = str($PlayerStats.souls)
	$SoulsSound.play()


func projectile_damage(value: float):
	stats.loose_health(value)
	$HitSound.play()


func _on_died() -> void:
	print("Player is dead")
	
	if undead: return
	root.death_sequence()
	
	$DeathSound.play()


func reset_death_vars():
	stats.reset_death_vars()
	$UI/Souls/Label.text = str(stats.souls)


func reset_vars():
	stats.reset_vars()
	
	velocity = Vector3.ZERO
	$CamV.rotation.x = 0
	
	target_height = max_collider_height
	was_on_floor = true
	prev_pos = global_position
	jump_pressed = false
	landing_effect_delay = 0
	footstep_delay = 0
	last_safe_pos = Vector3.ZERO
	
	release_aim()


func _on_last_safe_pos_timer_timeout() -> void:
	if is_on_floor():
		last_safe_pos = global_position


func recalc_quick_items():
	for i in range(1, quick_use_items.size() + 1):
		var item = str(quick_use_items[i-1])
		
		var icon: TextureRect = get_node("UI/Quick%d/Icon" % i)
		icon.visible = $Inventory.has_some(item)
		
		if icon.visible:
			icon.texture = root.get_loot(item).icon
		
		get_node("UI/Quick%d/Quantity" % i).text = "x" + str($Inventory.get_quantity(item))


func consume_quick_item(idx: int):
	var item = str(quick_use_items[idx])
	
	if $Inventory.has_some(item):
		$ConsumeSound.play()
		
		match item:
			"estus":
				var health_count = 15.0
				var step = 0.1
				var time = 2.5
				
				var tween = create_tween().set_loops(health_count / step)
				var delay = time / (health_count / step)
				tween.tween_callback(func(): $PlayerStats.gain_health(step)).set_delay(delay)
			"first_aid":
				var health_count = 30.0
				var step = 0.1
				var time = 2.0
				
				var tween = create_tween().set_loops(health_count / step)
				var delay = time / (health_count / step)
				tween.tween_callback(func(): $PlayerStats.gain_health(step)).set_delay(delay)
		
		$Inventory.take_item(item, 1)
		recalc_quick_items()


func choose_weapon(from_list, at_left_hand: bool):
	var actual_list = from_list.filter(func (x): return $Inventory.has_some(str(x)))
	
	if actual_list.size() == 0:
		$SoundWrongDisabled.play()
		return
	
	var hand = hand_left if at_left_hand else hand_right
	
	var idx = 0
	if hand in actual_list:
		idx = (actual_list.find(hand) + 1) % actual_list.size()
	
	if at_left_hand:
		if hand_right == actual_list[idx]:
			hand_right = hand_left
		hand_left = actual_list[idx]
	else:
		if hand_left == actual_list[idx]:
			hand_left = hand_right
		hand_right = actual_list[idx]
	
	recalc_weapons()


func recalc_weapons():
	if hand_right != null:
		$CamV/Camera3D/ArmRight/Cannon.set_visual(root.get_loot(hand_right).weapon_visual)
	else:
		$CamV/Camera3D/ArmRight/Cannon.set_visual(null)
		
	if hand_left != null:
		$CamV/Camera3D/ArmLeft/Cannon.set_visual(root.get_loot(hand_left).weapon_visual)
	else:
		$CamV/Camera3D/ArmLeft/Cannon.set_visual(null)


func has_weapon_right() -> bool:
	return $Inventory.has_some(str(hand_right))


func has_weapon_left() -> bool:
	return $Inventory.has_some(str(hand_left))


func start_aim():
	is_in_aim = true
	
	($CamV/Camera3D/ArmRight as Node3D).rotation = Vector3(0,0,0)
	
	var tween = create_tween()
	tween.tween_property($CamV/Camera3D/ArmRight, "position", Vector3(0,0,-0.408), 0.2)
	tween.parallel().tween_property($CamV/Camera3D, "fov", initial_fov * 0.6, 0.2)
	$UI/Crosshair.visible = false
	$UI/CrosshairFixed.visible = false


func release_aim():
	if is_in_aim:
		is_in_aim = false
		
		var tween = create_tween()
		tween.tween_property($CamV/Camera3D/ArmRight, "position", initial_hand_right_pos, 0.2)
		tween.parallel().tween_property($CamV/Camera3D, "fov", initial_fov, 0.2)
		$UI/Crosshair.visible = true
		$UI/CrosshairFixed.visible = true
