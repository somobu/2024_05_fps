extends StaticBody3D


@export var loot_entry_name = "something_stinky"


@onready var root: Root = get_node("/root/Root")
@onready var persist_key = "LOOT:" + str(self.get_path())

func _ready() -> void:
	if root.persist_data.has(persist_key):
		get_parent().remove_child.call_deferred(self)
		self.queue_free()


func action_get_name() -> String:
	return "Pick up"


func action_perform():
	get_parent().remove_child(self)
	self.queue_free()
	root.persist_data[persist_key] = true
	root.on_item_pick_up(loot_entry_name)
