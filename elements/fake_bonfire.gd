extends Node3D


func get_spawn_transform() -> Transform3D:
	return $SpawnPointMarker.global_transform
