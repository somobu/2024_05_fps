extends StaticBody3D

signal broken

@export var active_material: Material
@export var inactive_material: Material
@export var break_sound: AudioStream

var is_broken = false

func projectile_damage(value):
	is_broken = true
	emit_signal("broken")
	$OmniLight3D3.visible = false
	$Mesh.material_override = inactive_material
	$AudioStreamPlayer3D.stream = break_sound
	$AudioStreamPlayer3D.play()
