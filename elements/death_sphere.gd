class_name DeathSphere
extends StaticBody3D

@onready var player: Player = get_node("/root/Root/Player")

var souls: int = 0


func action_get_name() -> String:
	return "Acquire lost data"


func action_perform():
	if souls > 0:
		player.give_souls(souls)
	
	get_parent().remove_child(self)
	self.queue_free()
