extends StaticBody3D

signal broken

@export var active_red: StandardMaterial3D
@export var active_green: StandardMaterial3D
@export var inactive_material: StandardMaterial3D

@export var break_sound: AudioStream

var is_red_mode = true
var is_active = true
var is_broken = false

func _ready() -> void:
	$Mesh.material_override = active_red


func projectile_damage(value):
	is_broken = true
	emit_signal("broken")
	$OmniLight3D3.visible = false
	$FlareEffect.visible = false
	$Mesh.material_override = inactive_material
	$AudioStreamPlayer3D.stream = break_sound
	$AudioStreamPlayer3D.play()


func set_active_mode():
	is_active = true
	if not is_broken:
		$OmniLight3D3.visible = true
		$FlareEffect.visible = true
		$Mesh.material_override = active_red if is_red_mode else active_green


func set_inactive_mode():
	is_active = false
	if not is_broken:
		$OmniLight3D3.visible = false
		$FlareEffect.visible = false
		$Mesh.material_override = inactive_material


func set_green_mode():
	is_red_mode = false
	$FlareEffect.modulate = active_green.albedo_color
	$OmniLight3D3.light_color = active_green.albedo_color

func set_red_mode():
	is_red_mode = true
	$FlareEffect.modulate = active_red.albedo_color
	$OmniLight3D3.light_color = active_red.albedo_color
