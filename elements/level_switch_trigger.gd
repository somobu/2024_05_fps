extends Area3D

@export var is_load = true
@export var lvl_name = "movement_01"

@onready var lvl_loader: LevelLoader = get_node("/root/Root/LevelLoader")

func _on_body_entered(body: Node3D) -> void:
	if body is Player:
		if is_load:
			lvl_loader.load_level(lvl_name)
		else:
			lvl_loader.unload_level(lvl_name)
