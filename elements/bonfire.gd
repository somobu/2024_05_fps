class_name Bonfire
extends Node3D

class Data:
	## Name of .tscn if `levels` folder
	var level: String
	
	## Name of this node in level tree
	var id: String
	
	## Human-readable location name
	var human_name: String
	
	func save() -> Dictionary:
		return {
			"level": level,
			"id": id,
			"human_name": human_name
		}
	
	func load(data: Dictionary):
		level = data.level
		id = data.id
		human_name = data.human_name


const inactive_color = Color.RED
const active_color = Color.GREEN

@export var bonfire_name = "Unnamed"

@onready var root: Root = get_node("/root/Root")
@onready var player: Node3D = get_node("/root/Root/Player")

var is_activated = false
var lights_on = true


func _ready() -> void:
	is_activated = root.is_bonfire_activated(get_bonfire_dict())
	recalc()


func recalc():
	$Laptop/Label3D.text = "Ready" if is_activated else "Error"
	
	if is_activated:
		$Antenna/Lightbulb.set_green_mode()
		$Antenna/Lightbulb.set_active_mode()
	else:
		$Antenna/Lightbulb.set_red_mode()
	
	if is_activated and not $WireHum.playing:
		$WireHum.play()



func _on_antenna_tick_timer_timeout() -> void:
	if $Antenna/Lightbulb.is_broken:
		$AntennaTickTimer.stop()
		return
	
	if not is_activated:
		lights_on = not lights_on
		$Antenna/AudioStreamPlayer3D.play()
	
	if lights_on:
		$Antenna/Lightbulb.set_active_mode()
	else:
		$Antenna/Lightbulb.set_inactive_mode()


func set_activated_visual():
	is_activated = true
	lights_on = true
	recalc()


func get_bonfire_dict() -> Data:
	assert(get_parent().get_parent() is Root)
	
	var bd = Data.new()
	bd.level = get_parent().name
	bd.id = self.name
	bd.human_name = bonfire_name
	
	return bd


func get_spawn_transform() -> Transform3D:
	return $SpawnPointMarker.global_transform
