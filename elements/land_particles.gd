extends Node3D


func _ready() -> void:
	var p = $GPUParticles3D as GPUParticles3D
	p.one_shot = true
	p.restart()


func _on_gpu_particles_3d_finished() -> void:
	queue_free()
