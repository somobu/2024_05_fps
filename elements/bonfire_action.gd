extends Area3D

@onready var root: Root = get_node("/root/Root")
@onready var sp: Bonfire = get_parent()


func action_get_name() -> String:
	if sp.is_activated:
		return "Access comm sys"
	else:
		return "Activate comm sys"


func action_perform():
	if sp.is_activated:
		var dict = sp.get_bonfire_dict()
		var l = root.show_bonfire_menu.bind(dict)
		root.warp_sequence(dict, l)
		
		var tween = create_tween()
		tween.tween_callback(root.refill_estus_flasks).set_delay(0.6)
	else:
		get_parent().set_activated_visual()
		root.add_spawn_point_to_known(sp.get_bonfire_dict())
