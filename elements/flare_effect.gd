@tool
extends Node3D

@export var modulate: Color = Color.WHITE : set = _set_modulate, get = _get_modulate

var player: Node3D


func _ready():
	if not Engine.is_editor_hint():
		player = get_node("/root/Root/Player")


func _set_modulate(v: Color):
	modulate = v
	$Flare.modulate = v


func _get_modulate():
	return modulate


func _physics_process(_delta: float) -> void:
	if not Engine.is_editor_hint():
		$RayCast3D.target_position = to_local(player.global_position + Vector3(0, 1.5, 0))
		$Flare.visible = $RayCast3D.get_collider() == player
