extends Area3D

@export var delay = 10.0

signal triggered

func _on_body_entered(body: Node3D) -> void:
	if body is Player:
		var tween = create_tween()
		tween.tween_callback(func (): emit_signal("triggered")).set_delay(delay)
