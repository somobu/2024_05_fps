extends Node3D

@export var fire_rate: float = 3
@export var dispesion: Vector2 = Vector2(0.01, 0.01)
@export var dispersion_scale = 1.0
@export var bullet_speed: float = 180.0
@export var bullet_damage: float = 1.0

var firing = false
var fire_cooldown = 0

var projectile = preload("res://firearms/projectile.tscn")

var collision_parent = null

func _ready() -> void:
	var curr_parent = get_parent()
	while curr_parent is not PhysicsBody3D and not curr_parent.get_parent() == curr_parent:
		curr_parent = curr_parent.get_parent()
	collision_parent = curr_parent


func _physics_process(delta: float) -> void:
	fire_cooldown += delta
	
	if fire_cooldown < (1.0 / fire_rate):
		return
	
	if not firing:
		return
	
	fire_cooldown = 0
	
	var p: RigidBody3D = projectile.instantiate()
	p.damage = bullet_damage
	
	get_node("/root/Root").add_child(p)
	
	p.global_transform = self.global_transform
	p.global_position += to_global(Vector3(0,-0.02,0)) - to_global(Vector3(0,0,0))
	
	var da = randf_range(-dispesion.x, +dispesion.x) * dispersion_scale
	var db = randf_range(-dispesion.x, +dispesion.x) * dispersion_scale
	var direction = self.to_global(Vector3(da, db, -1.0)) - self.global_position
	p.linear_velocity = direction.normalized() * bullet_speed
	
	p.add_collision_exception_with(collision_parent)
	
	$AudioStreamPlayer3D.play()
	
	var firesplash_node: Node3D = $FireSplash
	if not firesplash_node.visible:
		firesplash_node.visible = true
		create_tween().tween_callback(firesplash_node.hide).set_delay(0.1)


func set_visual(visual: PackedScene):
	var old_v = get_node_or_null("Visual")
	if old_v != null:
		remove_child(old_v)
		old_v.queue_free()
	
	if visual == null:
		pass
	else:
		var v = visual.instantiate()
		v.name = "Visual"
		add_child(v)
