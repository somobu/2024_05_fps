extends RigidBody3D

var damage = 0.5

var decal = preload("res://firearms/bullet_decal.tscn")

var host: Node = null

var contact_pos

func _integrate_forces(state: PhysicsDirectBodyState3D):
	if(state.get_contact_count() >= 1):  #this check is needed or it will throw errors 
		contact_pos = state.get_contact_local_position(0)

func _on_body_entered(body: Node) -> void:
	
	if body.has_method("projectile_damage"):
		body.projectile_damage(damage)
	
	if not body is Player:
		var inst: Decal = decal.instantiate()
		body.add_child(inst)
		
		inst.global_transform = self.global_transform
		inst.global_position = contact_pos
		
		var mat_prop: SurfaceData = body.get_node_or_null("SurfaceData")
		if mat_prop == null:
			mat_prop = get_node("/root/Root/Player/DefaultSurfaceData")
		
		inst.get_node("AudioStreamPlayer3D").stream = mat_prop.bullet_sound
		inst.get_node("AudioStreamPlayer3D").play()
		
		if mat_prop.landing_effect != null:
			var l_eff = mat_prop.landing_effect.instantiate()
			inst.add_child(l_eff)
	
	get_parent().remove_child.call_deferred(self)
	self.queue_free()
