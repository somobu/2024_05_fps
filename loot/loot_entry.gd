class_name LootEntry
extends Node

enum Type {
		## Can be consumed (and assigned to a quick use slot)
	Consume,
	
	## Some kind of quest item
	Key,
	
	## Melee, Ranged, Explosive, Tool -- Can be carried in hands by player or enemy
	Weapon,
	
	Armor,
}

enum SubType {
	None,
	Weapon_Melee,
	Weapon_Ranged,
	Weapon_Explosive,
	Weapon_Tool,
	Armor_Helmet,
	Armor_Vest
}

@export var entry_type: Type = Type.Key
@export var entry_subtype: SubType = SubType.None

@export var weapon_visual: PackedScene = null

@export_category("Inventory Description")
@export var icon: Texture = null
@export var human_name: String = "Something stinky"
@export_multiline var description: String = "Piece of unidentifiable something. It smells really gross."
