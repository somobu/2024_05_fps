class_name LevelLoader
extends Node

var loaded_levels = []

func unload_all():
	for level in loaded_levels.duplicate():
		unload_level(level)


func load_level(lvl_name: String):
	load_level_f(lvl_name, func(): pass)


func load_level_f(lvl_name: String, callback: Callable):
	if get_parent().get_node_or_null(lvl_name) != null:
		assert(loaded_levels.count(lvl_name) == 1)
		return
	
	var scene = "res://levels/%s/root.tscn" % lvl_name
	
	var level_scene = load(scene)
	var level_node = level_scene.instantiate()
	level_node.name = lvl_name
	
	get_parent().add_child(level_node)
	loaded_levels.push_back(lvl_name)
	callback.call()


func unload_level(lvl_name: String):
	var level = get_parent().get_node_or_null(lvl_name)
	if level == null:
		assert(loaded_levels.count(lvl_name) == 0)
		return
	
	get_parent().remove_child(level)
	level.queue_free()
	
	loaded_levels.erase(lvl_name)
