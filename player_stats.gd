class_name PlayerStats
extends Node


signal died


const MAX_HEALTH = 20.0
const HEALTH_REGEN_SPEED = 0.0
var current_health = MAX_HEALTH


const MAX_STAMINA = 35.0

## Recovery speed, stamina units per second
const STAMINA_RECOVERY = 15.0

## Stamina consumption per jump
const STAMINA_PER_JUMP = 5.0

## Stamina consumption in sprint, units per second
const STAMINA_PER_SPRINT = 2.5

## Time before stamina regen start, seconds
const STAMINA_REGEN_DELAY = 0.9

var current_stamina = MAX_STAMINA
var stamina_regen_delay = 0


const MOVE_SPEED = 2.5
const SPRINT_SPEED = 5.0
const MAX_AIR_SPEED = 50.0
const CROUCH_SPEED = 0.7
const JUMP_VELOCITY = 4.2


var souls: int = 0


func _process(delta: float) -> void:
	
	# Regen health
	current_health += HEALTH_REGEN_SPEED * delta
	if current_health > MAX_HEALTH:
		current_health = MAX_HEALTH
	
	# Regen stamina
	stamina_regen_delay += delta
	if stamina_regen_delay > STAMINA_REGEN_DELAY:
		current_stamina += STAMINA_RECOVERY * delta
		current_stamina = clampf(current_stamina, 0, MAX_STAMINA)


func reset_vars():
	current_health = MAX_HEALTH
	current_stamina = MAX_STAMINA


func reset_death_vars():
	souls = 0


func get_health_percent() -> float:
	return current_health / MAX_HEALTH


func is_dead() -> bool:
	return current_health <= 0.0 and not get_parent().undead


func fall_damage(velocity: Vector3):
	if is_dead():
		print("Attempt to add more fall damage!")
		return
	
	var fall_damage_value = (velocity.y + 7.0) * 1.5
	fall_damage_value = clamp(fall_damage_value, fall_damage_value, 0)
	fall_damage_value = -pow(abs(fall_damage_value), 1.4) # Damage is negative
	current_health += fall_damage_value
	
	if current_health <= 0:
		died.emit()


func loose_health(value: float):
	assert(value > 0)
	
	if is_dead():
		return
	
	current_health -= value
	
	if current_health <= 0:
		died.emit()


func gain_health(value: float):
	assert(value > 0)
	
	if is_dead():
		return
	
	current_health = min(current_health + value, MAX_HEALTH)


func get_stamina_percent() -> float:
	return current_stamina / MAX_STAMINA


func has_stamina_for_jump() -> bool:
	return current_stamina > STAMINA_PER_JUMP


func is_low_stamina() -> bool:
	return current_stamina < 2 * STAMINA_PER_JUMP


func consume_jump_stamina():
	current_stamina -= STAMINA_PER_JUMP
	stamina_regen_delay = 0


func consume_sprint_stamina(delta: float, is_on_floor: bool):
	current_stamina -= STAMINA_PER_SPRINT * delta
	
	if is_on_floor:
		stamina_regen_delay = 0



func get_crouch_speed() -> float:
	return CROUCH_SPEED


func get_move_speed() -> float:
	return MOVE_SPEED


func get_sprint_speed() -> float:
	return SPRINT_SPEED


func get_jump_velocity() -> float:
	return JUMP_VELOCITY
