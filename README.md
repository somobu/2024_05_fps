# 2024.06 FPS

Some kind of first person shooter.

How to run: get godot 4.3 beta, import project and run (F5).

Note: some sounds are not included in repo. You have to manually fix imports before running the project.
